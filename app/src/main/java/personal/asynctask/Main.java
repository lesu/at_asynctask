package personal.asynctask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * <ul>
 * <li>Testing <a href="http://developer.android.com/reference/android/os/AsyncTask.html">AsyncTask</a></li>
 * </ul>
 */
public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }


    private void initComponents() {
        int max = Byte.MAX_VALUE;
        new BackgroudOperations(this).execute(max);
    }

}
