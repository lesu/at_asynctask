package personal.asynctask;


import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

final class BackgroudOperations extends AsyncTask<Integer,Integer,String>{
    private AppCompatActivity uiActivity;
    private ListView listView;
    private TextView textViewDisplay;
    private ArrayList<Integer> numbers = new ArrayList<Integer>();

    BackgroudOperations(AppCompatActivity uiActivity) {
        this.uiActivity = uiActivity;
        this.listView = (ListView) this.uiActivity.findViewById(R.id.list_view);
        this.textViewDisplay = (TextView) this.uiActivity.findViewById(R.id.text_view_display);
    }


    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected String doInBackground(Integer... params) {
        String result = "";

        int limit = new Random().nextInt(20);

        for (int i = 0; i < limit; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.e("InterruptedException",e.getMessage(),e);
            }
            int number = new Random().nextInt(params[0]);
            this.publishProgress(number);
            result = String.valueOf(number);
        }

        return result;
    }

    /**
     * Runs on the UI thread after {@link #publishProgress} is invoked.
     * The specified values are the values passed to {@link #publishProgress}.
     *
     * @param values The values indicating progress.
     * @see #publishProgress
     * @see #doInBackground
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        this.numbers.add(values[0]);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.uiActivity,R.layout.row,numbers);
        this.listView.setAdapter(arrayAdapter);
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param s The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(String result) {
        this.textViewDisplay.setText(result);
    }


}
